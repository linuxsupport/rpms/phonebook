%{!?dist: %define dist .slc5}
%define debug_package %{nil}

Name: phonebook
Summary: CERN LDAP personnel database client
Version: 1.11
Release: 2%{?dist}
Source: %{name}-%{version}.tgz

Group: CERN/Utilities
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Packager: Linux.Support@cern.ch
Vendor: CERN 
License: GPL
BuildArch: noarch
Requires: perl, perl-LDAP
BuildRequires: perl 


%if 0%{?rhel} >= 8
Requires: perl-Date-Manip
%endif

%description
phonebook is the CERN LDAP phonebook client.

%prep
%setup

%build
pod2man phonebook.pl > phonebook.1

%install
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
install -m 755 phonebook.pl $RPM_BUILD_ROOT/%{_bindir}/phonebook
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1
install -m 644 phonebook.1 $RPM_BUILD_ROOT/%{_mandir}/man1/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/phonebook
%{_mandir}/man1/phonebook.1*

%changelog
* Thu Nov 14 2019 Ben Morrice <ben.morrice@cern.ch> 1.11-2
- update for el8

* Mon Feb 13 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.11-1
- added --homedir HOMEDIR option (thanks Jan Iven for the patch)

* Fri May 20 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.10-1
- added --section ST option (thanks Jan Iven for the patch)

* Wed Sep 16 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.9.2-1
- fix uninitialized variable in 'yellowpages' view.

* Tue Jun 23 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.9.1-1
- new prefixes for CERN mobiles.

* Fri May 29 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.9-1
- sort the output by surname, add --orderby option

* Wed Sep 10 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.8.2-1
- fix for el7 scoped attr search (perl5.16 is much more strict 
  on quotes ...)

* Mon Jul 16 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.7-1
- added --terse secid option, thanks to A.Elwell fot the patch.

* Wed Jul 11 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.6.1-1
- added --ccid option , thanks to O.Raginel for the patch !
- added --gid, --uid options.

* Fri Nov 18 2011 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.5
- added 'yellowpages' option to query for services

* Tue Sep  6 2011 Jan van Eldik <Jan.van.Eldik@cern.ch> 1.4.3
- improve queries on group name (Trac #32)

* Wed Aug 31 2011 Jan van Eldik <Jan.van.Eldik@cern.ch> 1.4.2
- correct handling of queries returning >1000 results

* Wed Jan 19 2011 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.4
- rewrite to cope with changed ldap data (all accounts now have full records)

* Fri Jan 14 2011 Jaroslaw Polok <jaroslaw.polok@cern.ch> 1.3.1-1
- fixed terse option output for emails.

* Tue Nov 30 2010 Jan van Eldik <Jan.van.Eldik@cern.ch> 1.2-1
- distinguish (P)rimary, (S)econdary and (s)ervice accounts

* Wed Feb  3 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 1.0-1
- renamed package, make it noarch
- got rid of obsolete binaries
- cleaned up dependencies

* Wed Feb  3 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 3-7
- dummy transitional package

* Fri Jan 22 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.6.3
- fixed 'disappearing' persons sharing same names..

* Thu Jan 21 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.6.2
- added login option in man/help pages.

* Wed Jan 06 2010 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.6
- removed obsolete phone command.

* Thu Dec 10 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.5.5
- fixed searching by office/building

* Thu Nov 26 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.5.4
- added alternative search pattern for building/office
- added the wrapper for deprecating phone:
  http://cern.ch/ssb/ServiceChangeArchive/091130-Linux-Phonebook.htm

* Thu Nov 19 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.5.3
- fixed bug with uninitialized values in printf
- added -t uac and -t type options.

* Fri Nov 06 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.5.2
- fixed bug with some account info, added -t option

* Mon Nov 02 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.5.1
- better handling of additional people data (organization..etc)

* Fri Oct 30 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.5
- add accounts information, ccid, etc ... better matching.

* Wed Aug 19 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.4
- another fix. apparently we have non numeric phone
  numbers in databases ... (7XXXX ...)

* Tue Aug 18 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.3
- more fixes

* Wed Aug 13 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.2
- few fixes for searching by phone number

* Wed May 27 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.1
- changed phonebook to use xldap.cern.ch

* Thu Oct 09 2008 Jaroslaw Polok <jaroslaw.polok@cern.ch> 3.0
- provide more elaborated phonebook command (LDAP)

* Tue Oct 07 2008 Jan Iven <jan.iven@cern.ch> 2.1-99.cern
- deprecate "phone" command per wrapper as per IT-UDS
- add ldap-based "replacement"

* Wed Jul 02 2008 Jan Iven <jan.iven@cern.ch> 2.1-4.cern
- SLC5 version, .spec cleanup, remove old xwho

* Thu Aug 04 2004  Jan Iven <jan.iven@cern.ch> 2.1-3.cern
- changed ownership to root. me=idiot.

* Tue Jul 27 2004  Jan Iven <jan.iven@cern.ch> 2.1-2.cern
- replaced xwho by EOL message, old xwho is now xwho.deprecated, added
  phone man page

* Wed Aug 19 2003 Jaroslaw.Polok@cern.ch
- initial build


