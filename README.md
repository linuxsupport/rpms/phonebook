phonebook(1) queries the CERN LDAP personnel database.

## Deprecated

This package has been deprecated in favor of [pyphonebook](https://gitlab.cern.ch/linuxsupport/rpms/pyphonebook).
