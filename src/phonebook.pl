#!/usr/bin/perl -w

#
# lookup CERN LDAP for personal info
#
# 2017-02-13 v1.11.0 Jaroslaw.Polok <jaroslaw.polok@cern.ch>
#                       - add UnixHomeDir search option (thanks Jan Iven for patch)
# 2015-06-23 v1.9.1 Jaroslaw.Polok <jaroslaw.polok@cern.ch>
#                       - new prefixes for CERN mobiles.
# 2015-05-28 v1.9 Jaroslaw.Polok <jaroslaw.polok@cern.ch>
#                       - adding 'orderby' option.
# 2014-02-11 v1.8 Jaroslaw.Polok <jaroslaw.polok@cern.ch>
#			- additional attributes available in CERN LDAP 
# 2009-12-10 v1.0 Jaroslaw.Polok <jaroslaw.polok@cern.ch>

use strict;
use diagnostics;
use Getopt::Long;
use Text::Wrap;
use Pod::Usage;
use Date::Manip;
use Net::LDAP;
use Net::LDAP::Control::Paged;
use Net::LDAP::Constant qw( LDAP_CONTROL_PAGED );
#use Data::Dumper;

# apparently some people have phone numbers registered as 7XXXX ... ;-)
sub trph {
    my ($w,$ph)=@_;
    return "-" if (!$ph);
    if ($w) {
        if ($ph=~/^(\+|00)412276(\d\d\d\d\d)$/) { return $2;}
#        if ($ph=~/^(\+|00)4176487(\d\d\d\d)$/) { return "16".$2;}  
        if ($ph=~/^(\+|00)4175411(\d\d\d\d)$/) { return "16".$2;}
        if ($ph=~/^(\+|00)412276(7XXXX$)/) { return $2;}  
        return "-";
    } else {
#        if ($ph=~/^16(\d\d\d\d)$/) { return "+4176487".$1;}
        if ($ph=~/^16(\d\d\d\d)$/) { return "+4175411".$1;}
        if ($ph=~/^(\d\d\d\d\d)$/) { return "+412276".$1;}
        if ($ph=~/^(7XXXX)$/) { return "+412276".$1;}
    }
    return $ph;
}

# ugly? : see values at http://support.microsoft.com/kb/305144
sub decuac {
    my ($uac)=@_;
    my ($ret)="?";
    return $ret if !defined($uac);
    return "D" if ($uac & 2); #disabled account, 
    return "P" if ($uac & 8388608); #password expired
    return "L" if ($uac & 16); #lockout
    return "A" if ($uac & 512); #normal account , active
    return $ret; 
    
}

sub dectype {
    my ($type)=@_;
    my ($ret)="?";
    return $ret if !defined($type);
    return "P" if ($type eq 'Primary');
    return "S" if ($type eq 'Secondary');
    return "s" if ($type =~/^(CRA Service Provider)|(Service)$/);
    return "U";
}

my (%allpeople,%allgroups,@allgids);

my $ldapserver="xldap.cern.ch";

my $shortusage="Usage: phonebook SEARCHSTRING. See: phonebook --help for more information\n";

my ($firstname,$surname,$email,$building,$office,$login,$department,$group,$section,$all,$debug,$help,$phone,$mobile,$fax,$ext,$ypages,$pobox,$ccid,$uid,$gid,$homedir,@terse,$sort);
my %opts=(
          "firstname=s"   => \$firstname,
          "surname=s"     => \$surname,
          "email=s"       => \$email,
          "building=s"    => \$building,
          "office=s"      => \$office,
          "login=s"       => \$login,
          "department=s"  => \$department,
          "group=s"       => \$group,
          "section=s"     => \$section,
          "phone=s"       => \$phone, 
          "fax=s"         => \$fax,
          "mobile=s"      => \$mobile,
          "all"           => \$all,
          "externals"     => \$ext,
          "yellowpages"   => \$ypages,
          "pobox=s"       => \$pobox,
          "ccid=s"        => \$ccid,
	  "uid=s"	  => \$uid,
	  "gid=s"	  => \$gid,
	  "homedir=s"	  => \$homedir,
          "terse=s@"      => \@terse, 
          "verbose"       => \$debug,
	  "orderby=s"     => \$sort, 
          "help"          => \$help,
);

my @tersenames = qw (SURNAME FIRSTNAME PHONE MOBILE FAX OFFICE DEPARTMENT GROUP CERNEXTERNALMAIL CERNGROUP CERNSECTION POBOX EMAIL DISPLAYNAME ORGANIZATION CCID LOGIN HOMEDIR LAST UID GID EMAILNICK COMPANY CERNLOWUID SHELL OTHERPHONE UAC TYPE SECID HOMEDIR);

my @orderedby = qw (SURNAME FIRSTNAME DEPARTMENT GROUP LOGIN CERNGROUP CERNSECTION DISPLAYNAME);
 
my $search="";
my $ok =0;
my $options = GetOptions(%opts);

if (@terse && $all) {
    printf "Error: -t(terse) and -a(ll) options are mutually exclusive, see phonebook --help for help.\n"; exit(1); 
}

if ($ypages && $ext) {
    printf "Error: -e(xternals) and -y(ellowpages) options are mutually exclusive, see phonebook --help for help.\n"; exit(1); 
}

pod2usage(-verbose=> 2) if $help;

foreach my $t (@terse) {
    $t=uc($t);
    if (!grep(/^$t$/, @tersenames)) {
        printf "Error: Unrecognized -t(erse) option value ($t), see phonebook --help for help.\n"; exit(1);
    }
}

if ($sort) {
 $sort=uc($sort);
 if (!grep(/^$sort$/, @orderedby)) {
     printf "Error: Unrecognized -sort(by) option value ($sort), see phonebook --help for help.\n"; exit(1);  
 }
} else {
 $sort='SURNAME';
}

while (@ARGV){
    if ($ARGV[0]=~/^\d+$/) {
        $search.="(|(mobile=*".trph(0,$ARGV[0]).")(telephoneNumber=*".trph(0,$ARGV[0])."))";$ok+=1;shift @ARGV;
    } else {
        $search.="(|(displayName=*$ARGV[0]*)(sAMAccountName=$ARGV[0]))";$ok+=1;shift @ARGV;
    }
}
# sn=* eliminates 'persons' where no name is registered...
$search="(&".$search."(sn=*))" if $ok;

$search.="(sAMAccountName=$login)",$ok+=1 if $login;
$search.="(givenName=$firstname)",$ok+=1 if $firstname;
$search.="(sn=$surname)",$ok+=1 if $surname;
$search.="(|(physicalDeliveryOfficeName=Bld*$building *)(physicalDeliveryOfficeName=$building *))",$ok+=1 if $building;
$search.="(|(physicalDeliveryOfficeName=*Room*$office*)(physicalDeliveryOfficeName=* *$office*))",$ok+=1 if $office;
$search.="(division=$department)",$ok+=1 if $department;
$search.="(|(mail=$email)(proxyAddresses=smtp:$email))",$ok+=1 if $email;
$search.="(department=*/$group)",$ok+=1 if $group;
$search.="(cernSection=$section)",$ok+=1 if $section;
$search.="(telephoneNumber=*".trph(0,$phone).")",$ok+=1 if $phone;
$search.="(facsimileTelephoneNumber=*".trph(0,$fax).")",$ok+=1 if $fax;
$search.="(mobile=*".trph(0,$mobile).")",$ok+=1 if $mobile;
$search.="(postOfficeBox=$pobox)",$ok+=1 if $pobox;
$search.="(employeeID=$ccid)",$ok+=1 if $ccid;
$search.="(uidNumber=$uid)", $ok+=1 if $uid;
$search.="(gidNumber=$gid)", $ok+=1 if $gid;
$search.="(unixHomeDirectory=$homedir)", $ok+=1 if $homedir;
 

do {printf $shortusage;exit 1} if !$ok;

$search="(&".$search.")" if $ok > 1;

printf "LDAP search: %s\n",$search if $debug;

my $endline="No match found.\n";

my $ldap = Net::LDAP->new ( $ldapserver , version => 3, multihomed => 1, timeout => 15 ) or die "Error connecting to LDAP: $@\n";
my $page = Net::LDAP::Control::Paged->new( size => 1000 );

my $mesg = $ldap->bind or die "Error binding to LDAP: $@\n";

# we search only active CERN users: ou=users,ou=organic units,dc=cern,dc=ch
# unless--ext is spefified.
# or for ext. users ?              : ou=externals,dc=cern,dc=ch
# or for services ?                : ou=services,dc=cern,dc=ch

my $base="ou=users,ou=organic units,dc=cern,dc=ch";
$base="ou=externals,dc=cern,dc=ch" if ($ext);
$base="ou=services,dc=cern,dc=ch" if ($ypages);
my @args = (base    => $base,
	    scope   => "sub",
	    filter  => "$search",
	    control => [$page],
	    attrs   => "",
            );


my $cookie;
my @entries = ();
while(1) {

    my $result = $ldap->search (@args) or die "Error searching LDAP: $@\n";
    
    die "LDAP error: ".$result->code." - ".$result->error_name.": ".$result->error_text."\n" if $result->code && $result->code != 4;

    push(@entries,$result->entries);
    #print ">>> ".scalar(@entries)." found ...\n";
    # Get cookie from paged control
    my($resp)  = $result->control( LDAP_CONTROL_PAGED ) or last;
    $cookie    = $resp->cookie or last;
   
    # Set cookie in paged control
    $page->cookie($cookie);

    #last if $debug;
}

if ($cookie) {
    # We had an abnormal exit, so let the server know we do not want any more
    $page->cookie($cookie);
    $page->size(0);
    $ldap->search( @args );
}

printf "LDAP return: %d entries.\n",scalar(@entries) if $debug;
#exit;

my ($guniqid)=-100000;

foreach my $entr (@entries) {
    my ($uniqid,$dn)=undef; 
	
    $uniqid=$entr->get_value('employeeID'); 
    $uniqid=$guniqid-- if (!defined($uniqid));     
	
    $dn=$entr->get_value('distinguishedName');
    # OK, this really should not happen (which most likely means it will - sooner ot later ;-))
    next if (!defined($dn));
	
    printf "--LDAP entry--\n" if $debug;
	
    if ($debug) {
	foreach my $attr (sort $entr->attributes) {
	    print $attr."=>";
	    my @out1=$entr->get_value ( $attr );
	    foreach my $out (@out1) { 
		if ($out!~/^[[:ascii:]]+$/) { print "0x".unpack("H*",$out)."\n"; } else { print $out."\n"; }
	    }
	}
    }
    
    @{$allpeople{$uniqid}{$dn}{OBJECTCLASS}}=$entr->get_value('objectClass');
    $allpeople{$uniqid}{$dn}{SURNAME}=$entr->get_value('sn');
    $allpeople{$uniqid}{$dn}{FIRSTNAME}=$entr->get_value('givenName');
    $allpeople{$uniqid}{$dn}{PHONE}=$entr->get_value('telephoneNumber');
    $allpeople{$uniqid}{$dn}{MOBILE}=$entr->get_value('mobile');
    $allpeople{$uniqid}{$dn}{FAX}=$entr->get_value('facsimileTelephoneNumber');
    $allpeople{$uniqid}{$dn}{OFFICE}=$entr->get_value('physicalDeliveryOfficeName');
    $allpeople{$uniqid}{$dn}{DEPARTMENT}=$entr->get_value('division');
    $allpeople{$uniqid}{$dn}{GROUP}=$entr->get_value('extensionAttribute13');
    $allpeople{$uniqid}{$dn}{POBOX}=$entr->get_value('postOfficeBox');
    $allpeople{$uniqid}{$dn}{DISPLAYNAME}=$entr->get_value('displayName');
    $allpeople{$uniqid}{$dn}{ORGANIZATION}=$entr->get_value('extensionAttribute11');
    $allpeople{$uniqid}{$dn}{LAST}=$entr->get_value('extensionAttribute14');
    $allpeople{$uniqid}{$dn}{CCID}=$entr->get_value('employeeID');
    $allpeople{$uniqid}{$dn}{LOGIN}=$entr->get_value('sAMAccountName');
    $allpeople{$uniqid}{$dn}{SHELL}=$entr->get_value('loginShell');
    $allpeople{$uniqid}{$dn}{HOMEDIR}=$entr->get_value('unixHomeDirectory');
    $allpeople{$uniqid}{$dn}{EMAILNICK}=$entr->get_value('mailNickname');
    $allpeople{$uniqid}{$dn}{TYPE}=$entr->get_value('employeeType');
    $allpeople{$uniqid}{$dn}{UID}=$entr->get_value('uidNumber');
    $allpeople{$uniqid}{$dn}{GID}=$entr->get_value('gidNumber');
    $allpeople{$uniqid}{$dn}{UAC}=$entr->get_value('userAccountControl');
    @{$allpeople{$uniqid}{$dn}{COMPANY}}=split('-',$entr->get_value('company')) if(defined($entr->get_value('company')));
    #$allpeople{$uniqid}{$dn}{COMPANY}=$entr->get_value('company');
    @{$allpeople{$uniqid}{$dn}{OTHERPHONE}}=$entr->get_value('otherTelephone');
    $allpeople{$uniqid}{$dn}{EMAIL}=$entr->get_value('mail');
    $allpeople{$uniqid}{$dn}{WWWHOMEPAGE}=$entr->get_value('wWWHomePage');
    $allpeople{$uniqid}{$dn}{INFO}=$entr->get_value('info');
    $allpeople{$uniqid}{$dn}{CERNSECTION}=$entr->get_value('cernSection');
    $allpeople{$uniqid}{$dn}{CERNGROUP}=$entr->get_value('cernGroup');
    $allpeople{$uniqid}{$dn}{CERNEXTERNALMAIL}=$entr->get_value('cernExternalMail');
    $allpeople{$uniqid}{$dn}{CERNINSTITUTEABBREVIATION}=$entr->get_value('cernInstituteAbbreviation');
    $allpeople{$uniqid}{$dn}{CERNINSTITUTENAME}=$entr->get_value('cernInstituteName');
    $allpeople{$uniqid}{$dn}{CERNLOWUID}=$entr->get_value('cernLowUid');
    $allpeople{$uniqid}{$dn}{OFFICE}=~s/^(Bld\.\s+)|(Room)//g if (defined($allpeople{$uniqid}{$dn}{OFFICE}));

    @{$allpeople{$uniqid}{$dn}{SECID}}=$entr->get_value('altSecurityIdentities');
    
    push(@allgids,$allpeople{$uniqid}{$dn}{GID}) if defined($allpeople{$uniqid}{$dn}{GID});
}

print "Unique employeeID's: ".scalar(keys %allpeople)." entries\n" if $debug;
# Let's get group names for all gids ...

if ($all && ($#entries+1) && ($#allgids+1)) {
    $search="";
    foreach my $gid (keys %{{map{$_=>1} @allgids}}) { $search.="(gidNumber=$gid)"; }
    $search="(&(|".$search.")(objectClass=group))"; 
    
    printf "LDAP search: %s\n",$search if $debug;
    
    my $result = $ldap->search ( base    => "ou=unix,ou=workgroups,dc=cern,dc=ch",
                              scope   => "one",
                              filter  => "$search",
                              attrs   => ['cn','gidNumber']) or die "Error searching LDAP: $@\n";
    

    die "LDAP error: ".$result->code." - ".$result->error_name.": ".$result->error_text."\n" if $result->code && $result->code != 4;

    my @entries2 = $result->entries;

    printf "LDAP return: %d entries.\n",$#entries2+1 if $debug;

    foreach my $entr ( @entries2 ) {
        printf "--LDAP entry--\n" if $debug;
        my ($attr,$group,$gid) = undef;
        foreach my $attr ( sort $entr->attributes ) {
            #this would have even worked if that wouldn;t be an MS ldap ...
            next if ( $attr =~ /;binary$/ );
            if ($debug) {
                print $attr."=>";
                my @out1=$entr->get_value ( $attr );
                foreach my $out (@out1) {
                    if ($out!~/^[[:ascii:]]+$/) { print "0x".unpack("H*",$out)."\n"; } else { print $out."\n"; }
                }
            }
            for ($attr) {
                /^cn$/
                    && do { $group=$entr->get_value ( $attr ); last;};
                /^gidNumber$/
                    && do { $gid=$entr->get_value ( $attr ); last;};
            }
        }
        $allgroups{$gid}=$group if ($group && $gid);
    }
}

my $accs=0;
    
foreach my $uniqid (keys %allpeople) {
   foreach my $dn (keys %{$allpeople{$uniqid}}) { 
      if (!defined($allpeople{$uniqid}{$dn}{$sort})) { 
         $allpeople{$uniqid}{$dn}{$sort}=""; 
      }
   }
}

my @alluniqids;

foreach my $keypair (
   sort { lc($allpeople{$a->[0]}{$a->[1]}{$sort}) cmp lc($allpeople{$b->[0]}{$b->[1]}{$sort}) }
   map { my $intk=$_; map [ $intk, $_ ], keys %{$allpeople{$intk}} } keys %allpeople ) 
   {
      push (@alluniqids,$keypair->[0]);
   }  
 
@alluniqids = do { my %seen; grep { !$seen{$_}++ } @alluniqids };

printf "#------------------------------------------------------------------------------\n" if $all && ($#entries+1);
  foreach my $uniqid (@alluniqids) {
    my ($primary)=undef;
    foreach my $dn (sort keys %{$allpeople{$uniqid}}) {
       $primary=$dn;  
       last if(defined($allpeople{$uniqid}{$dn}{TYPE}) && $allpeople{$uniqid}{$dn}{TYPE} eq 'Primary'); 
    }

        my $whatsthis = ${$allpeople{$uniqid}{$primary}{OBJECTCLASS}}[3]; # 'contact' or 'user'
        my (@allphones)=();        
        my (@tmpoph)=();

        foreach my $dn (sort keys %{$allpeople{$uniqid}}) {
          push(@allphones,$allpeople{$uniqid}{$dn}{PHONE}) if (defined($allpeople{$uniqid}{$dn}{PHONE}));
#          foreach my $ph ($allpeople{$uniqid}{$dn}{OTHERPHONE}
        push(@allphones,@{$allpeople{$uniqid}{$dn}{OTHERPHONE}}) if (@{$allpeople{$uniqid}{$dn}{OTHERPHONE}});
        }
        
        my %tmph= map {$_,1} @allphones;

        foreach my $ph (keys %tmph) {
            next if (length($ph) == 5); # we handle short ones ourselves
            my($pph)=undef;
            if (defined($allpeople{$uniqid}{$primary}{PHONE})) { $pph=$allpeople{$uniqid}{$primary}{PHONE}; } else {$pph = 0;}
            if (defined($ph) && $ph ne $pph) {
                push(@tmpoph,$ph);
            }
        }

  if ($all) {
        if ($whatsthis eq 'contact') {
         printf "Service:            %-.59s\n",$allpeople{$uniqid}{$primary}{DISPLAYNAME} || "-";
         printf "Service:            %-.59s\n",${$allpeople{$uniqid}{$primary}{COMPANY}}[0] || "-";
         printf "E-mail:             %-.59s\n",$allpeople{$uniqid}{$primary}{EMAIL} || "-";
        } else {
         #       12345678901234567890
         printf "Surname:            %-.59s\n",$allpeople{$uniqid}{$primary}{SURNAME} || "-";
         printf "Firstname:          %-.59s\n",$allpeople{$uniqid}{$primary}{FIRSTNAME} || "-";
         printf "Display Name:       %-.59s\n",$allpeople{$uniqid}{$primary}{DISPLAYNAME} || "-";
         printf "E-mail:             %-.59s\n",$allpeople{$uniqid}{$primary}{EMAIL} || "-";
        }
       foreach my $dn (sort keys %{$allpeople{$uniqid}}) {
          next if ($dn eq $primary);
          if ($allpeople{$uniqid}{$dn}{EMAIL} && 
              $allpeople{$uniqid}{$primary}{EMAIL} && 
              $allpeople{$uniqid}{$dn}{EMAIL} !~m/^$allpeople{$uniqid}{$primary}{EMAIL}$/i) {
                printf "Other E-mail:       %-.59s\n",$allpeople{$uniqid}{$dn}{EMAIL} || "-"; }

       }
        printf "External E-mail:    %-.59s\n",$allpeople{$uniqid}{$primary}{CERNEXTERNALMAIL} || "-" 
          if (defined($allpeople{$uniqid}{$primary}{CERNEXTERNALMAIL}) && $allpeople{$uniqid}{$primary}{EMAIL} !~m/^$allpeople{$uniqid}{$primary}{CERNEXTERNALMAIL}$/i); 
   
        printf "Telephone:          %-41.25s (internal: %6.6s)\n",$allpeople{$uniqid}{$primary}{PHONE} || "-",
            trph(1,$allpeople{$uniqid}{$primary}{PHONE}) || "-";
      
        foreach my $tmpoph1 (@tmpoph) {       
          printf "Other telephone:    %-41.25s (internal: %6.6s)\n",$tmpoph1 || "-",
                trph(1,$tmpoph1) || "-";
        }     
        
        printf "Mobile:             %-41.25s (internal: %6.6s)\n",$allpeople{$uniqid}{$primary}{MOBILE} || "-",
            trph(1,$allpeople{$uniqid}{$primary}{MOBILE}) || "-";
        printf "Facsimile:          %-41.25s (internal: %6.6s)\n",$allpeople{$uniqid}{$primary}{FAX} || "-",
            trph(1,$allpeople{$uniqid}{$primary}{FAX}) || "-";

        if ($whatsthis eq 'contact') {
         $Text::Wrap::columns= 80;
         printf "Bld. Floor-Room:    %-25.25s\n",$allpeople{$uniqid}{$primary}{OFFICE} || "-";
         printf fill("Information:        ","                    ",$allpeople{$uniqid}{$primary}{INFO} || "-")."\n"; 
         printf fill("WWW:                ","                    ",$allpeople{$uniqid}{$primary}{WWWHOMEPAGE} || "-")."\n";
        } else {
         printf "Department:         %-.59s\n",$allpeople{$uniqid}{$primary}{DEPARTMENT} || "-";
         printf "Group:              %-.59s\n",$allpeople{$uniqid}{$primary}{CERNGROUP} || "-";
         printf "Section:            %-.59s\n",$allpeople{$uniqid}{$primary}{CERNSECTION} || "-";
         printf "POBox:              %-41.25s (internal mail)\n",$allpeople{$uniqid}{$primary}{POBOX} || "-";
         printf "Bld. Floor-Room:    %-25.25s\n",$allpeople{$uniqid}{$primary}{OFFICE} || "-";
         printf "Organization:       %-.59s\n",$allpeople{$uniqid}{$primary}{ORGANIZATION} || "-";

         foreach my $part (@{$allpeople{$uniqid}{$primary}{COMPANY}}) {
            # thank you CRA(P) ! for the messy data: CERN-CERN-CERN or Unknown-Unknown-Unknown ...
            next if $part =~/^CERN|Unknown$/; 
            printf "Organization:       %-.59s\n",$part; 
        }
        
        printf "Computer Center ID: %-6.6s\n",$allpeople{$uniqid}{$primary}{CCID} || "-";
        printf "Low user ID:        %-6.6s\n",$allpeople{$uniqid}{$primary}{CERNLOWUID} || "-";
     
# OK, so I know that comparing length to check if this is real account or not is not very bright ..
# but then what other choice do I have ?
# fortunately in coming years (decades ? ;-)) we are going to limit usernames to 8 chars .. so 20 chars is not 'real' username ...
                
        printf ("\nComputer account(s):\nLogin    Grp St Uid   Gid  Last login    Shell    Home directory \n\n") if ($allpeople{$uniqid}{$primary}{LOGIN} && (length($allpeople{$uniqid}{$primary}{LOGIN}) ne 20)); 

        printf "%-8.8s %-3.8s %-2.2s %5.6s %4.4s %-14.14s %-9.9s %-28.28s\n",  
                $allpeople{$uniqid}{$primary}{LOGIN} || "-",
                defined($allpeople{$uniqid}{$primary}{GID}) && $allgroups{$allpeople{$uniqid}{$primary}{GID}} || "-", 
                dectype($allpeople{$uniqid}{$primary}{TYPE}).decuac($allpeople{$uniqid}{$primary}{UAC}) || "??",
                $allpeople{$uniqid}{$primary}{UID} || "-",
                $allpeople{$uniqid}{$primary}{GID} || "-",
                $allpeople{$uniqid}{$primary}{LAST} && UnixDate(ParseDate($allpeople{$uniqid}{$primary}{LAST}),"%d/%m/%y %H:%M") || "--/--/-- --:--",
                $allpeople{$uniqid}{$primary}{SHELL} || "-",
                $allpeople{$uniqid}{$primary}{HOMEDIR} || "-" if ($allpeople{$uniqid}{$primary}{LOGIN} && (length($allpeople{$uniqid}{$primary}{LOGIN}) ne 20));
                $accs+=1 if ($allpeople{$uniqid}{$primary}{LOGIN} && (length($allpeople{$uniqid}{$primary}{LOGIN}) ne 20));

        for my $dn (keys %{$allpeople{$uniqid}}) {
           
            next if ($allpeople{$uniqid}{$primary}{LOGIN} && $allpeople{$uniqid}{$dn}{LOGIN} eq $allpeople{$uniqid}{$primary}{LOGIN});
            $accs+=1;
            
            printf "%-8.8s %-3.8s %-2.2s %5.5s %4.4s %-14.14s %-9.9s %-28.28s\n",
                $allpeople{$uniqid}{$dn}{LOGIN} || "-",
                defined($allpeople{$uniqid}{$dn}{GID}) && $allgroups{$allpeople{$uniqid}{$dn}{GID}} || "-", 
                dectype($allpeople{$uniqid}{$dn}{TYPE}).decuac($allpeople{$uniqid}{$dn}{UAC}) || "??",
                $allpeople{$uniqid}{$dn}{UID} || "-",
                $allpeople{$uniqid}{$dn}{GID} || "-",
                $allpeople{$uniqid}{$dn}{LAST} && UnixDate(ParseDate($allpeople{$uniqid}{$dn}{LAST}),"%d/%m/%y %H:%M") || "--/--/-- --:--",
                $allpeople{$uniqid}{$dn}{SHELL} || "-",
                $allpeople{$uniqid}{$dn}{HOMEDIR} || "-";
        }
        } # whatis
        printf "#-------------------------------------------------------------------------------\n" if $all && ($#entries+1);
      
}#all 
elsif (@terse) {

 foreach my $dn (keys %{$allpeople{$uniqid}}) { 
    foreach my $t (@terse) { 
      if (ref($allpeople{$uniqid}{$dn}{uc($t)}) eq "ARRAY") {
        if (uc($t) =~ /^SECID$/) {
          printf "[%s];",join('],[',@{$allpeople{$uniqid}{$dn}{uc($t)}}) || "-";
        } else {
          printf "%s;",join(',',@{$allpeople{$uniqid}{$dn}{uc($t)}}) || "-";
        }
      } else {
        if (uc($t) =~ /^SECID$/) {
          printf "%s;",'['.$allpeople{$uniqid}{$dn}{uc($t)}.']' || "[-]";
        } else {
          printf "%s;",$allpeople{$uniqid}{$dn}{uc($t)} || "-";
        }
      } 
    }
    printf "\n";
    }
} else {
       # foreach my $dn (sort keys %{$allpeople{$uniqid}}){
        my $dn=$primary;
       if ($whatsthis eq 'contact') {
        printf "%-40.40s %-5.5s %-5.5s %-6.6s %-3.3s %-3.3s %-3.3s %11.11s\n",
            $allpeople{$uniqid}{$dn}{DISPLAYNAME} || "-",
            trph(1,$allpeople{$uniqid}{$dn}{PHONE}) || "-",
            trph(1,$tmpoph[0]) || "-", # stupid .. we get out first one ... if multiple .. well, use -a to get'em all ..
            trph(1,$allpeople{$uniqid}{$dn}{MOBILE}) || "-",
            $allpeople{$uniqid}{$dn}{DEPARTMENT} || "-",$allpeople{$uniqid}{$dn}{GROUP} || "-",$allpeople{$uniqid}{$dn}{CERNSECTION} || "-",
            $allpeople{$uniqid}{$dn}{OFFICE} || "-";
       } else {
        printf "%-24.24s %-15.15s %-5.5s %-5.5s %-6.6s %-3.3s %-3.3s %-3.3s %11.11s\n",
            $allpeople{$uniqid}{$dn}{SURNAME} || "-",$allpeople{$uniqid}{$dn}{FIRSTNAME} || "-",
            trph(1,$allpeople{$uniqid}{$dn}{PHONE}) || "-",
            trph(1,$tmpoph[0]) || "-", # stupid .. we get out first one ... if multiple .. well, use -a to get'em all ..
            trph(1,$allpeople{$uniqid}{$dn}{MOBILE}) || "-",
            $allpeople{$uniqid}{$dn}{DEPARTMENT} || "-",$allpeople{$uniqid}{$dn}{GROUP} || "-", $allpeople{$uniqid}{$dn}{CERNSECTION} || "-",
            $allpeople{$uniqid}{$dn}{OFFICE} || "-";
       # }
       }
    }

}


        printf "#Account St(atus): P(rimary), S(econdary), s(ervice), U(nknown)\n" if $all && $accs;
        printf "#                  A(ctive),  D(isabled),  P(assword expired),  L(ocked out)\n" if $all && $accs;

#printf "[output truncated - maximum number of LDAP records reached (1000).]\n" if ($#entries + 1 == 1000) ;
__END__

=pod

=head1 NAME

phonebook - Utility to search CERN phone book.

=head1 DESCRIPTION

phonebook is a replacement for aging phone command well known to all CERN unix users.
It will search CERN LDAP database for user information and display the output according
to search criteria defined.

=head1 SYNOPSIS

=over 2

	phonebook [--help]
  
	phonebook SEARCHSTRING [--all]
  
	phonebook [SEARCHSTRING] [--surname SN] [--firstname FN] 
	[--building BD] [--office FL-OFF] [--email M@A]
	[--phone PH] [--mobile MB]
	[--department DP] [--group GP] [--section ST ] [--pobox PB]
	[--login LN] [--ccid ID] [--uid UID] [--gid GID]
	[--externals] [--yellowpages] [--all|--terse TTT] [--orderby OOO]

=back

=head1 OPTIONS



=over 4

=item B<--help>

Shows this help description

=item B<--surname> SURNAME

Search for people with SURNAME.

=item B<--firstname> FIRSTNAME

Search for people with FIRSTNAME.

=item B<--building> BUILDING

Search for people with offices in BUILDING.

=item B<--office> FLOOR-OFFICE | OFFICE | FLOOR

Search for people with offices FLOOR-OFFICE.

=item B<--email> EMAIL@ADDRESS

Search for people having EMAIL@ADDRESS.

=item B<--login> LOGIN

Search for people whose computer LOGIN matches.

=item B<--department> DEPARTMENT

Search for people employed in DEPARTMENT.

=item B<--group> GROUP 

Search for people employed in GROUP.

=item B<--section> SECTION 

Search for people employed in SECTION.

=item B<--phone> PHONE

Search for people whose PHONE matches.

=item B<--mobile> MOBILE

Search for people whose MOBILE matches.

=item B<--pobox> POBOX

Search for people whose POBOX matches.

=item B<--ccid> COMPUTER-CENTER-ID

Search for people whose computer center ID matches.

=item B<--uid> UID

Search for people whose computer User ID matches.

=item B<--gid> GID

Search for people whose computer Group ID matches.

=item B<--homedir> HOMEDIR

Search for people whose home directory matches. Please note that LDAP wildcards may 
need to be escaped to protect them from the shell.

=item B<--externals>

search only for people having external / lightweight accounts registered.

=item B<--yellowpages>

search for services instead of people

=item B<--all> 
     
Show all fields in the output.

=item B<--orderby> OOO

Order the output sorting (alphanumerically) values of one of the fields. 

Valid OOO values is: surname, firstname, login, department, group, cerngroup, cernsection or displayname.

=item B<--terse> TTT

Show only TTT fields in the output. This output option allows to
fully customize what phonebook prints in the output listing.

Valid TTT values are: surname, firstname, phone, otherphone, mobile, fax, office, department, group, pobox, email, displayname, organization, ccid, login, homedir, last, uid, gid, uac, type, emailnick, company, shell ,secid.

Terse option can be specified multiple times on the command line.
Output values are listed in same order as input terse options and are separated 
by ';' sign. Multiple-value values are separated by ','. Empty values are
printed as '-'.

Names of most of the terse option arguments are self-explanatory,
except:

uac - user account control: this field returns value of UAC field
      in Active Directory in decimal and permits to check if the
      account is blocked and for what reason: to decode the value 
      please see: http://support.microsoft.com/kb/305144

type - account type: value can be P - for primary account, S - for
       secondary account, s - for service account

secid - alternate security identities mapped to the account 
       (ssh keys, X509 certificates, YubiKey(s) .. etc)

       Note: identifies are printed out as:

       '[TYPE:Content]' where TYPE can be SSH,X509,YubiKey 
                        and Content value depends on type.

This option could be useful for easy parsing the phonebook output and
use it as input for other programs. 
     
=back

All options can be abbreviated to shortest distinctive lenght,
(first letter in most cases). Single minus preceding option 
name may be used  instead of double one.

For wildcard search use * character (double quoted or back-slashed 
to avoid shell variable expansion: "*" or \* ).

Please note that search option arguments are case insensitive.

=head1 DESCRIPTION

phonebook performs a search in CERN LDAP database, according to 
search criteria defined by options.  

=head1 EXAMPLES

	phonebook someperson
	phonebook "*som*erson"
	phonebook -dep IT
	phonebook oneperson "*con*d*son" -group FIO
	phonebook -phone 7\*XXX
        phonebook -a firstname surname
        phonebook surname firstname -t surname -t firstname -t email 

=head1 AUTHOR

Jaroslaw Polok <jaroslaw.polok@cern.ch>

=head1 KNOWN BUGS

Unlike the old phone command phonebook searches for partial matches too,
ie: phonebook 'someperson' returns same results as 'phonebook epers'.

Number of returned results is limited to 1000. (this is rather a feature).

Searches returning large number of results can take long time (about 10 seconds).

=cut
      

 


